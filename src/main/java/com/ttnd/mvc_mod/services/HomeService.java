package com.ttnd.mvc_mod.services;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ttnd.mvc_mod.co.UserCO;
import com.ttnd.mvc_mod.entity.Employee;
import com.ttnd.mvc_mod.entity.Role;
import com.ttnd.mvc_mod.entity.User;
import com.ttnd.mvc_mod.repository.EmployeeRepository;
import com.ttnd.mvc_mod.repository.RoleRepository;
import com.ttnd.mvc_mod.repository.UserRepository;
import com.ttnd.mvc_mod.util.RoleConstant;

@Service
@PropertySource(value="classpath:/application.properties")
public class HomeService implements ApplicationContextAware{
	
	ApplicationContext context;
	
	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context=context;
		
	}
	
	@Value("${message}")
	String message;
	
	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	UserRepository userRepository;
	
	
	@Autowired 
	BCryptPasswordEncoder encoder;
	
		
	
	public String getMessage() {
		Employee employee=new Employee();
	    employeeRepository.saveOrUpdateUsingSessionFactory(employee);
	    Employee employee1=new Employee();
        employeeRepository.saveOrUpdateUsingHT(employee1);
	    
	    
	    /*SessionFactory factory=new LocalSessionFactoryBuilder(dataSource).buildSessionFactory();
	    //SessionFactory sessionFactory=new AnnotationSessionFactoryBean();
	    SessionFactory sessionFactory=new LocalSessionFactoryBean();
	    
	    factory.setDataSource(dataSource);*/
	    
	    
	    
		return message;
	}



	public void createUser(UserCO userCO,String roleString) {
		Role role=roleRepository.findRoleByName(roleString);
		User user= new User();
		user.setUsername(userCO.getEmail().split("@")[0]);
		user.setEmail(userCO.getEmail());
		user.setName(userCO.getName().toString());
		user.setPassword(encoder.encode("123"));
		user.setDob(new Date());
		
		userRepository.createOrUpdateUser(user,role);
	}
	
	public void persistsRoles() {
		roleRepository.persistRoles();
	}



	public void test() {
		 employeeRepository.test();
		
	}



	public String getAuthToken(String uname, String password) {
		User user=userRepository.findByUserName(uname);
		String token="unauthenticated";
		if(BCrypt.checkpw(password,user.getPassword())){
			token=user.getToken().toString();
		}
		return token;
	}







	

}
