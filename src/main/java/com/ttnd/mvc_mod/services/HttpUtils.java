package com.ttnd.mvc_mod.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class HttpUtils {

    public static String postData(String testBackendUrl, String authToken, String requestMethod) throws Exception {
        StringBuffer response = new StringBuffer();
        try {
            URL obj = new URL(testBackendUrl);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod(requestMethod);

            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            int responseCode = con.getResponseCode();
            
            
            //readStream(con, responseCode);
            read(con);
            
            
            
        }catch (IOException e) {
            System.out.println("Could not process Request : " + e.getMessage());
        }
        return null;
    }
    
    
    static void readStream(HttpURLConnection con,int responseCode ){
		
		try{
        StringBuffer response= new StringBuffer();      
        System.out.println("GET Response Code :: " + responseCode);
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());

        } else {
            System.out.println("GET request not worked");
        }
        String responseStr = response.toString();
		}catch (Exception e) {
		}
		
	}
	
	static void read(HttpURLConnection con){
		
		try {
			/*JAXBContext jc;
			jc = JAXBContext.newInstance(ConsumedRest.class);
			ConsumedRest consumedRest =(ConsumedRest) jc.createUnmarshaller().unmarshal(con.getInputStream());
			*/
			/*
			Map<String, Object> properties = new HashMap<String, Object>(1);
	        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");*/
			
			
	        JAXBContext jc = JAXBContext.newInstance(new Class[] {ConsumedRest.class});

	        Unmarshaller unmarshaller = jc.createUnmarshaller();
	        ConsumedRest objectA = (ConsumedRest) unmarshaller.unmarshal(con.getInputStream());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


    public static void main(String[] args) throws Exception {
    String restGetUrl="https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A141038944&start-date=30daysAgo&end-date=yesterday&metrics=ga%3AnewUsers%2Cga%3Asessions%2Cga%3Aimpressions%2Cga%3AadClicks&access_token=ya29.Glz6A2zfG5VFkpmK3cxBHIksB2p6_RBVdLgnl6o1TyZSNcYe02vrhiG1K9vSPBBR72pVIPUrcOUGHzTNeBsUlQFi6aStYiu7w-s41IGfc50hgXhnXx29Zh1PwFz0FQ";
    
    
    
    postData(restGetUrl,"", "GET");
    }



}
