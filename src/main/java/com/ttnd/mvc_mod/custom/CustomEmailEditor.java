package com.ttnd.mvc_mod.custom;

import java.beans.PropertyEditorSupport;


public class CustomEmailEditor extends PropertyEditorSupport{
	
	@Override
	public void setAsText(String email) throws IllegalArgumentException {
		if(!email.contains("@")){
			email=email.concat("@gmail.com");
		}
		super.setValue(email);
	}

}
