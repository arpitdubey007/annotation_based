package com.ttnd.mvc_mod.custom;

import org.springframework.context.ApplicationContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.GenericFilterBean;
import com.ttnd.mvc_mod.entity.User;
import com.ttnd.mvc_mod.repository.UserRepository;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;

public class CustomFilter extends GenericFilterBean {

	@Autowired
	UserRepository userRepository;

	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		User user = null;
		String authToken = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		System.out.println("CustomFilter Invoked------------" + authToken);
		if (authToken != null && !authToken.isEmpty()) {
			Set<GrantedAuthority> authorities = new HashSet<>();
			if (userRepository == null) {
				ServletContext servletContext = request.getServletContext();
				WebApplicationContext webApplicationContext = WebApplicationContextUtils
						.getWebApplicationContext(servletContext);
				userRepository = webApplicationContext.getBean(UserRepository.class);
			}
			user = userRepository.findUserWithToken(authToken);
			if (user != null) {
				user.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getRoleName())));		
				System.out.println(user);
				user.getRoles().forEach(role -> System.out.println("Role     :"+role.getRoleName()));
				authorities.forEach(action->System.out.println("Authority: "+action.getAuthority()));
				UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user, "",
						authorities);
				SecurityContextHolder.getContext().setAuthentication(token);
				chain.doFilter(request, response);
			} else {
				HttpServletResponse httpServletResponse = (HttpServletResponse) response;
				httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
			}
		} else {
			SecurityContextHolder.getContext().setAuthentication(null);
			chain.doFilter(request, response);
		}
	}

}