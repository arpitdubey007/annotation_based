package com.ttnd.mvc_mod.co;

import org.springframework.stereotype.Component;

@Component
public class NameCO {
	
	String title;
	String fname;
	String lname;
	
	public String getTitle() {
		return title;
	}
	public NameCO setTitle(String title) {
		this.title = title;
		return this;
	}
	public String getFname() {
		return fname;
	}
	public NameCO setFname(String fname) {
		this.fname = fname;
		return this;
	}
	public String getLname() {
		return lname;
	}
	public NameCO setLname(String lname) {
		this.lname = lname;
		return this;
	}
	
	
	@Override
	public String toString() {
		return this.title.concat(" ").concat(this.fname).concat(" ").concat(this.lname);
	}
	

}
