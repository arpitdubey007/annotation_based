package com.ttnd.mvc_mod.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(basePackages = {"com.ttnd.mvc_mod.controller","com.ttnd.mvc_mod.custom"})
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter {
	@Bean
    public ViewResolver getViewResolver(){
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }
	
	@Bean
	public AnnotationInterceptor annotationInterceptor(){
		return new AnnotationInterceptor();
	}

	 @Override
	 public void addInterceptors(InterceptorRegistry registry) {
	     registry
	     .addInterceptor((HandlerInterceptor) annotationInterceptor())
	     .addPathPatterns("/**")
         .excludePathPatterns("/**/*.ecxld"); 
	 }

}
