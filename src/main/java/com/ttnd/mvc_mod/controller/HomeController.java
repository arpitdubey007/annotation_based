package com.ttnd.mvc_mod.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ttnd.mvc_mod.co.UserCO;
import com.ttnd.mvc_mod.custom.CustomEmailEditor;
import com.ttnd.mvc_mod.entity.User;
import com.ttnd.mvc_mod.services.HomeService;
import com.ttnd.mvc_mod.util.RoleConstant;

@Controller
public class HomeController{
	
	@Autowired
	HomeService homeService;
	
	
	
	@InitBinder
	public void initBind(WebDataBinder binder){
		binder.registerCustomEditor(String.class,"email", new CustomEmailEditor());
		binder.registerCustomEditor(Date.class, "dob", new CustomDateEditor(new SimpleDateFormat("dd-mm-yyyy"), false));
	}
	
	@RequestMapping(value="/")
	public ModelAndView app() throws IOException{
		homeService.persistsRoles();
		return new ModelAndView("registration","message","Welcome");
	}
	
	
	
	@RequestMapping(value="/403")
	public ModelAndView accessDeniedPage(HttpServletResponse response) throws IOException{
		return new ModelAndView("home","message","403");
	}

	@RequestMapping(value="/login")
	public ModelAndView login(HttpServletResponse response) throws IOException{
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		System.out.println("Login Page !"+name);
		return new ModelAndView("login","message","Login Page");
	}
	
	@RequestMapping(value="/login/{message}")
	public ModelAndView login(@PathVariable String message,HttpServletResponse response) throws IOException{
		System.out.println("Message :   "+message);
		return new ModelAndView("login","message",message +" Page");
	}
	
	
	
	@RequestMapping(value="/register")
	public ModelAndView register(@Valid UserCO userCO,BindingResult bindingResult) throws IOException{
		
	String message;
		if(bindingResult.hasErrors()){
			message=bindingResult.getErrorCount()+ ": Error in binding!\n";
			bindingResult.getAllErrors().stream().forEach(error->{
				
				System.out.println(error+"  :   "+userCO.getEmail());
			}
			);
			
		}else{
			message="binded successfully!!";
		homeService.createUser(userCO,RoleConstant.ADMIN.getRole());
		}
		return new ModelAndView("home","message","Home Page  "+message);
	}
	
	
	@RequestMapping(value="/readProperty")
	public ModelAndView readProperty(HttpServletResponse response) throws IOException{
		return new ModelAndView("home","message",homeService.getMessage());
	}
	
	
	@RequestMapping(value="/ads")
	public ModelAndView ads(HttpServletResponse response) throws IOException{
		return new ModelAndView("home","message",homeService.getMessage());
	}
	
	@Secured({"ROLE_USER"})
	@RequestMapping(value="/methodLevelSecurity")
	public ModelAndView methodLevelSecurity(HttpServletResponse response) throws IOException{
		return new ModelAndView("home","message","MethodLevelSecurity");
	}
	
	
	@RequestMapping(value="/flash")
	public String flash(Model model,RedirectAttributes rm) throws IOException{
		UserCO userCO= new UserCO();
		userCO.setEmail("rptdbay@gmail.com");
		rm.addFlashAttribute("flashkey", "flashvalue");
		rm.addFlashAttribute("userCO", userCO);
	    rm.addAttribute("attributekey1", "attributeValue");
	    rm.addAttribute("flashkey", "attributeValuekeynameinflash");
		return "redirect:/controller1";
	}
	
	@RequestMapping(value="/controller1")
	public String app1(Model model,HttpServletRequest request) throws IOException{
		System.out.println("=====================================");
		System.out.println("In Controller 1");
		model.addAttribute("nonflash", "nonflash in C1");
		Map md = model.asMap();
	    for (Object modelKey : md.keySet()) {
	        Object modelValue = md.get(modelKey);
	        System.out.println("Model data =="+ modelKey + " -- " + modelValue);
	    }

	    java.util.Enumeration<String> reqEnum = request.getParameterNames();
	    while (reqEnum.hasMoreElements()) {
	        String s = reqEnum.nextElement();
	        System.out.println("Request data =="+ s+" : "+ request.getParameter(s));
	    }
		
		
		return "redirect:/controller2";
	}
	
	
	
	@RequestMapping(value="/controller2")
	public String app2(Model model,HttpServletRequest request) throws IOException{
		System.out.println("=====================================");
		System.out.println("In Controller 2");
		Map md = model.asMap();
	    for (Object modelKey : md.keySet()) {
	        Object modelValue = md.get(modelKey);
	        System.out.println("Model data =="+ modelKey + " -- " + modelValue);
	    }

	    java.util.Enumeration<String> reqEnum = request.getParameterNames();
	    while (reqEnum.hasMoreElements()) {
	        String s = reqEnum.nextElement();
	        System.out.println("Request data =="+ s+" : "+ request.getParameter(s));
	    }
		
		
		return "redirect:/controller3";
	}
	
	
	
	@RequestMapping(value="/controller3")
	public ModelAndView app3() throws IOException{
		return new ModelAndView("registration","message","Welcome");
	}
	
	

}
