package com.ttnd.mvc_mod.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ttnd.mvc_mod.co.AuthenticateCO;
import com.ttnd.mvc_mod.services.HomeService;

@RestController
@RequestMapping(value = "/")
public class ApiController {
	
	@Autowired
	HomeService homeService;
	
	@Secured("hasRole('ROLE_USER')")
	@RequestMapping(value = "/getUserJson",method = RequestMethod.GET)
    public ResponseEntity<String> getJson() throws Exception{
        HttpStatus httpStatus=HttpStatus.OK;
        String message="sucess";
        return new ResponseEntity<String>(message, httpStatus);
    }
	
	@Secured("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/getAdminJson",method = RequestMethod.GET)
    public ResponseEntity<String> getAdminJson() throws Exception{
		System.out.println("Current User---------  "+SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		SecurityContextHolder.getContext().getAuthentication().getAuthorities().forEach(action->System.out.println("Current Users Authorities-------"+action.getAuthority()));
        HttpStatus httpStatus=HttpStatus.OK;
        String message="sucess";
        return new ResponseEntity<String>(message, httpStatus);
    }  
	
	
	
	@RequestMapping(value = "/authenticate",method = RequestMethod.GET)
    public ResponseEntity<String> authenticate(AuthenticateCO authenticateCO) throws Exception{
        HttpStatus httpStatus=HttpStatus.OK;
        String uname=authenticateCO.getUsername();
        String password=authenticateCO.getPassword();
        String auth_token=homeService.getAuthToken(uname,password);
        return new ResponseEntity<String>(auth_token, httpStatus);
    }
	
	

}
