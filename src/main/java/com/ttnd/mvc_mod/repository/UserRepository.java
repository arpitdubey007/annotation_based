package com.ttnd.mvc_mod.repository;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ttnd.mvc_mod.entity.Role;
import com.ttnd.mvc_mod.entity.User;

@Repository
public class UserRepository {
	
	@Autowired
    private SessionFactory sessionFactory;
	
    @Transactional
	public User findByUserName(String userName) {		
		Session session=sessionFactory.openSession();
		User user = (User) session.get(User.class, userName);
		return user;
	}

    @Transactional
	public  void createOrUpdateUser(User user,Role role) {
    	user.getRoles().add(role);
    	role.getUsers().add(user);
    	Session session=sessionFactory.getCurrentSession();
    	session.saveOrUpdate(user);
    	session.merge(role);
	}
    
    
    @Transactional
	public User findByFieldHavingValue(String field,String value) {	
    	Session session=sessionFactory.getCurrentSession();
    	Criteria criteria = session.createCriteria(User.class);
    	User user = (User) criteria.add(Restrictions.eq(field, value)).uniqueResult();
		return user;
	}
    
    
    @Transactional
	public User findUserWithToken(String token) {
    	User user=null;
    	try{
    		Session session=sessionFactory.getCurrentSession();
        	Criteria criteria = session.createCriteria(User.class);
        	user = (User) criteria.add(Restrictions.eq("token", token)).uniqueResult();
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    		user=null;
    	}
    	
		return user;
	}
    
    
    
    

}
