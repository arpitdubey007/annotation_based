package com.ttnd.mvc_mod.repository;

import java.util.List;

import javax.annotation.PostConstruct;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ttnd.mvc_mod.entity.Airline;
import com.ttnd.mvc_mod.entity.Employee;
import com.ttnd.mvc_mod.entity.Flight;

@Repository
public class EmployeeRepository {
	
	@Autowired
    private SessionFactory sessionFactory;
	
	
	@Autowired
	HibernateTemplate hibernateTemplate;
    
    @Transactional
    public void saveOrUpdateUsingSessionFactory(Employee employee) {
    	Session session=sessionFactory.openSession();
        session.saveOrUpdate(employee);
        int id=employee.getId();
		employee.setUsername("username_"+id);
		employee.setPassword("sessionFactory"+id);
		employee.setEmail("email-"+id+"@gmail.com");
		System.out.println("----------------!");
		
    }
    
    
    @Transactional
    public List<Employee> getAllUsers()
    {
        return this.hibernateTemplate.loadAll(Employee.class);
    }
    
    @Transactional
    public Integer saveOrUpdateUsingHT(Employee employee)
    {
    	Employee mergedEmployee = this.hibernateTemplate.merge(employee);
        int id = mergedEmployee.getId();
        mergedEmployee.setUsername("username_"+id);
        mergedEmployee.setPassword("hibernateTemplate"+id);
        mergedEmployee.setEmail("email-"+id+"@gmail.com");
        return id;
    }
    
    
    
    @PostConstruct
	public void post(){
		System.out.println("EmployeeRepository bean is scaned and registered by @ComponentScan!....");
	}


	public void test() {
		Airline airline= new Airline();
		
		Flight flight1= new Flight();
		flight1.setName("flight1");
		//flight1.setAirline(airline);
		
		Flight flight2= new Flight();
		flight2.setName("Del-NJ");
		//flight.setAirline(airline);
		
		airline.setName("Air India");
		airline.getFlights().add(flight1);
		airline.getFlights().add(flight2);
		Session session=sessionFactory.openSession();
		session.saveOrUpdate(airline);
		//session.saveOrUpdate(flight);
		
	}
 

}
