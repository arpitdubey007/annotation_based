<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
</head>
<body>
	<p>${message}</p>


	<sec:authorize access="isAuthenticated()">
		<a href="<c:url value="/j_spring_security_logout" />">Logout</a>
	</sec:authorize>
</body>





<%-- <sec:authorize access="hasRole('ROLE_ANONYMOUS')">
    
    </sec:authorize> --%>



<%--  <sec:authorize access="isAnonymous()">
    
    </sec:authorize> --%>



<%-- <sec:authorize ifAnyGranted="ROLE_USER">
        ROLE_USER
    </sec:authorize>
    
    <sec:authorize ifNotGranted="ROLE_ADMIN">
        ROLE_ADMIN
    </sec:authorize> --%>


<%--  --%>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script language="JavaScript">
	window.onbeforeunload = function() {
		
		return "closing";
	}
</script>

</html>
