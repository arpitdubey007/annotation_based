<!DOCTYPE html>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<html>
<head>
  <link href='http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
  <link href='//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css' rel='stylesheet' type='text/css'>
  <link href='//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/1.8/css/bootstrap-switch.css' rel='stylesheet' type='text/css'>
  <link href='http://davidstutz.github.io/bootstrap-multiselect/css/bootstrap-multiselect.css' rel='stylesheet' type='text/css'>
  <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
  <script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js' type='text/javascript'></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.0/js/bootstrap.min.js' type='text/javascript'></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js' type='text/javascript'></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/1.8/js/bootstrap-switch.min.js' type='text/javascript'></script>
  <script src='http://davidstutz.github.io/bootstrap-multiselect/js/bootstrap-multiselect.js' type='text/javascript'></script>
  <script src='${pageContext.request.contextPath}/resources/js/app.js' type='text/javascript'></script>
  
</head>

<body>

<%-- <spring:message code="message"/> --%>
<spring:eval expression="@environment.getProperty('message')" />

<div class='col-md-7'></div>
<div class='col-md-5'>

  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <h5>User - Registration</h5>
      </div>
      <div class='panel-body'>
        <form class='form-horizontal' role='form' action="<c:url value='/register'/>" method="GET">
          
          <div class='form-group'>
            <label class='control-label col-md-2' for='id_title'>Name</label>
            <div class='col-md-10'>
              <div class='col-md-2'>
                <div class='form-group internal'>
                  <select class='form-control' id='id_title' name="name.title">
                    <option>Mr</option>
                    <option>Ms</option>
                    <option>Mrs</option>
                    <option>Miss</option>
                    <option>Dr</option>
                  </select>
                </div>
              </div>
              <div class='col-md-4 indent-small'>
                <div class='form-group internal'>
                  <input class='form-control' id='id_first_name' placeholder='First Name' type='text' name="name.fname">
                </div>
              </div>
              <div class='col-md-4 indent-small'>
                <div class='form-group internal'>
                  <input class='form-control' id='id_last_name' placeholder='Last Name' type='text' name="name.lname">
                </div>
              </div>
            </div>
          </div>
          <div class='form-group'>
            <label class='control-label col-md-2' for='id_adults'>Age</label>
            <div class='col-md-10'>
              <div class='col-md-3'>
                <div class='form-group internal'>
                  <input class='form-control col-md-6' id='id_adults' placeholder='Age' type='number' name="age">
                </div>
              </div>
  
            </div>
          </div>
          <div class='form-group'>
            <label class='control-label col-md-2' for='id_email'>Contact</label>
            <div class='col-md-6'>
              <div class='form-group'>
                <div class='col-md-11'>
                  <input class='form-control' id='id_email' placeholder='E-mail' type='text' name="email">
                </div>
              </div>
              <div class='form-group internal'>
                <div class='col-md-11'>
                  <input class='form-control' id='id_phone' placeholder='Phone: (xxx) - xxx xxxx' type='text' name="phone">
                </div>
              </div>
            </div>
          </div>
          <div class='form-group'>
            <label class='control-label col-md-2'>DOB</label>
            <div class='col-md-10'>
              <div class='col-md-6'>
                <div class='form-group internal input-group'>
                  <input class='form-control datepicker' name="dob">
                  <span class='input-group-addon'>
                    <i class='glyphicon glyphicon-calendar'></i>
                  </span>
                </div>
              </div>
              
            </div>
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-2'>DOJ</label>
            <div class='col-md-10'>
              <div class='col-md-6'>
                <div class='form-group internal input-group'>
                  <input class='form-control datepicker'>
                  <span class='input-group-addon'>
                    <i class='glyphicon glyphicon-calendar'></i>
                  </span>
                </div>
              </div>
              
            </div>
          </div>
          
          <div class='form-group'>
            <label class='control-label col-md-2' for='id_pets'>Active</label>
            <div class='col-md-10'>
              <div class='make-switch' data-off-label='NO' data-on-label='YES' id='id_pets_switch'>
                <input id='id_pets' type='checkbox' name="active">
              </div>
            </div>
          </div>
          <div class='form-group'>
            <label class='control-label col-md-2' for='id_comments'>Comments</label>
            <div class='col-md-6'>
              <textarea class='form-control' id='id_comments' placeholder='Additional comments' rows='3'></textarea>
            </div>
          </div>
          <div class='form-group'>
            <div class='col-md-offset-1 col-md-3'>
              <button class='btn-lg btn-primary' type='submit'>Register</button>
            </div>
            <div class='col-md-3'>
              <button class='btn-lg btn-danger' style='float:right' type='submit'>Cancel</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  </div>


<script>
$(document).ready(function() {  
	  $('.multiselect').multiselect();
	  $('.datepicker').datepicker();  
	});
</script>

</body>


</html>
